:: run.bat

:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::
SET JAVA_HOME=C:\jdk1.3.1_15
SET java=%JAVA_HOME%\bin\java.exe
SET api_lib_dir=src\main\resources
:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::

SET coreapi=%api_lib_dir%\coreapi.jar
SET utilityapi=%api_lib_dir%\utilityapi.jar
SET testapi=%api_lib_dir%\testapi.jar
SET corba_sun=%api_lib_dir%\corba_sun.jar

%java% -classpath target\classes;%coreapi%;%utilityapi%;%testapi%;%corba_sun% sample.Main