:: configure.bat

:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::
SET JAVA_HOME=C:\jdk1.3.1_15
SET javac=%JAVA_HOME%\bin\javac.exe
SET api_lib_dir=src\main\resources
:: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: :: ::

SET coreapi=%api_lib_dir%\coreapi.jar
SET utilityapi=%api_lib_dir%\utilityapi.jar
SET testapi=%api_lib_dir%\testapi.jar
SET corba_sun=%api_lib_dir%\corba_sun.jar

mkdir \a target\classes

%javac% -d target/classes -sourcepath src/main/java/sample -classpath .;%coreapi%;%utilityapi%;%testapi%;%corba_sun% src/main/java/sample/*.java

pause