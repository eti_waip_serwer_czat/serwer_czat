
Sposób prosty:

1. Ściągnij i zainstaluj JDK 1.3.1_15 (https://bitbucket.org/eti_waip_serwer_czat/serwer_czat/downloads)
2. Pobierz źródła projektu (można przez gita albo przez downloads - link wyżej)
3. Otwórz projekt w IntelliJ IDEA (https://www.jetbrains.com/idea/download/)
4. Uruchom simulator.jar i Main (prawy górny róg IDE)
5. W aplikacji serwisu kliknij start
6. W symulatorze stwórz nowy telefon i wyślij SMS na nr 2222
7. Odczytaj SMSa zwrotnego

Sposób standardowy:

1. Zainstaluj "Network Resource Gateway SDK" http://eti2011.pl/index.php?topic=5643.0; potrzeba JDK w wersji 1.3 (installer windowsowy instaluje z automatu)
2. Pobierz źródła projektu (można przez gita albo przez downloads)
5. Uruchom run_simulator.bat w katalogu "Network Resource Gateway SDK" (na win7: C:\Program Files (x86)\Ericsson\Network Resource Gateway SDK\R5A02)
6. Uruchom serwis (w plikach projektu compile.bat -> run.bat)
7. W aplikacji serwisu kliknij start
8. W symulatorze stwórz nowy telefon i wyślij SMS na nr 2222
9. Odczytaj SMSa zwrotnego