package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;
import pl.edu.pg.eti.waip.chat.server.logic.messengers.Messenger;
import pl.edu.pg.eti.waip.chat.server.logic.rooms.ChatRoom;

import java.util.Iterator;
import java.util.Set;

public class SayCommandService implements CommandService {
    private final CommandServicesResource resource;

    public SayCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        String messageContent = message.content;
        if (messageContent.startsWith("/")) {
            messageContent = messageContent.substring(messageContent.indexOf(" ") + 1);
        }

        Messenger senderMessenger = resource.messengerManager.getMessengerByNumber(message.sender);
        String lineToSend = senderMessenger.name + ": " + messageContent;

        ChatRoom chatRoomWithMessenger = resource.chatRoomManager.getChatRoomWithMessenger(message.sender);
        Set messengers = chatRoomWithMessenger.getListeningMessengers();
        Iterator iterator = messengers.iterator();
        while (iterator.hasNext()) {
            String messengerId = (String) iterator.next();
            System.out.println("sending to: " + messengerId);
            if (!message.sender.equals(messengerId)) {
                resource.messageSender.send(lineToSend, messengerId);
            }
        }

        resource.messagesHistory.storeForRoom(chatRoomWithMessenger.name, lineToSend);

        System.out.println(message.sender + ": " + messageContent);
    }
}
