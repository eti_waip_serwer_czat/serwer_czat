package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;

import java.util.StringTokenizer;

public class ChangeNameCommandService implements CommandService {
    private final CommandServicesResource resource;

    public ChangeNameCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        StringTokenizer stringTokenizer = new StringTokenizer(message.content);
        String command = (String) stringTokenizer.nextElement();
        String param1 = (String) stringTokenizer.nextElement();

        String oldName = resource.messengerManager.setMessengerName(message.sender, param1);

        System.out.println("changed name for " + message.sender + " from " + oldName + " to " + param1);
    }
}
