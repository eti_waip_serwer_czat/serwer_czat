package pl.edu.pg.eti.waip.chat.server.logic.commands;

import pl.edu.pg.eti.waip.chat.server.MessageSender;
import pl.edu.pg.eti.waip.chat.server.logic.MessagesHistory;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.ChangeNameCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.CommandHelpCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.JoinRoomCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.ListRoomHistoryCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.ListRoomsCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.SayCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.services.WhoIsInRoomCommandService;
import pl.edu.pg.eti.waip.chat.server.logic.messengers.MessengerManager;
import pl.edu.pg.eti.waip.chat.server.logic.rooms.ChatRoomManager;

import java.util.HashMap;
import java.util.Map;

public class CommandServicesResource {

    public final MessageSender messageSender;
    public final MessengerManager messengerManager;
    public final ChatRoomManager chatRoomManager;
    public final MessagesHistory messagesHistory;

    private Map/*<String, CommandService>*/ serviceMap;

    public CommandServicesResource(MessageSender messageSender, MessengerManager messengerManager, ChatRoomManager chatRoomManager, MessagesHistory messagesHistory) {
        this.messageSender = messageSender;
        this.messengerManager = messengerManager;
        this.chatRoomManager = chatRoomManager;
        this.messagesHistory = messagesHistory;

        serviceMap = new HashMap();
        serviceMap.put("say", new SayCommandService(this));
        serviceMap.put("change-name", new ChangeNameCommandService(this));
        serviceMap.put("join-room", new JoinRoomCommandService(this));
        serviceMap.put("list-rooms", new ListRoomsCommandService(this));
        serviceMap.put("who-in-room", new WhoIsInRoomCommandService(this));
        serviceMap.put("history", new ListRoomHistoryCommandService(this));
        serviceMap.put("help", new CommandHelpCommandService(this));
    }

    public CommandService getCommandService(String commandServiceName) {
        return (CommandService) serviceMap.get(commandServiceName);
    }
}
