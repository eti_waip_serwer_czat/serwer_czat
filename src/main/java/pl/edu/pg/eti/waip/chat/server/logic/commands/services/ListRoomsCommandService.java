package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;

import java.util.Iterator;
import java.util.Set;

public class ListRoomsCommandService implements CommandService {
    private final CommandServicesResource resource;

    public ListRoomsCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        StringBuffer messageBuilder = new StringBuffer();

        Set chatRooms = resource.chatRoomManager.getChatRooms();
        Iterator iterator = chatRooms.iterator();
        while (iterator.hasNext()) {
            String chatRoomName = (String) iterator.next();
            messageBuilder.append(chatRoomName);
            messageBuilder.append(ChatServer.ENDL);
        }
        resource.messageSender.send(messageBuilder.toString(), message.sender);

        System.out.println(message.sender + " listed rooms");
    }
}