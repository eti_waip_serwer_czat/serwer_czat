package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;
import pl.edu.pg.eti.waip.chat.server.logic.messengers.Messenger;

import java.util.StringTokenizer;

public class JoinRoomCommandService implements CommandService {
    private final CommandServicesResource resource;

    public JoinRoomCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        StringTokenizer stringTokenizer = new StringTokenizer(message.content);
        String command = (String) stringTokenizer.nextElement();
        String param1 = (String) stringTokenizer.nextElement();

        resource.chatRoomManager.moveMessengerToRoom(message.sender, param1);

        System.out.println(message.sender + " joined room " + param1);
    }
}