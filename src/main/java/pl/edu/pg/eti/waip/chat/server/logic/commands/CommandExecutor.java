package pl.edu.pg.eti.waip.chat.server.logic.commands;

public class CommandExecutor {

    private CommandServicesResource commandServicesResource;

    public CommandExecutor(CommandServicesResource commandServicesResource) {
        this.commandServicesResource = commandServicesResource;
    }

    public void execute(Command command) {
        commandServicesResource.getCommandService(command.commandServiceName).execute(command.message);
    }
}
