package pl.edu.pg.eti.waip.chat.server.logic.commands;

import pl.edu.pg.eti.waip.chat.server.Message;

public class Command {
    public final String commandServiceName;
    public final Message message;

    public Command(String commandServiceName, Message message) {
        this.commandServiceName = commandServiceName;
        this.message = message;
    }
}
