package pl.edu.pg.eti.waip.chat.server.app;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.MessageSender;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ChatApp {
    private final static String CALL_NUMBER = "2222";

    private GUI gui;
    private SmsHandler smsHandler;
    private ChatServer chatServer;

    public ChatApp(String configFile) throws IOException {
        gui = new GUI();

        Properties frameworkProperties = new Properties();
        frameworkProperties.load(new FileInputStream(configFile));
        smsHandler = new SmsHandler(frameworkProperties, CALL_NUMBER);

        chatServer = new ChatServer(new MessageSender() {
            public void send(String messageContent, String receiver) {
                smsHandler.sendSMS(CALL_NUMBER, receiver, messageContent);
            }
        });

        gui.addButton(new AbstractAction("Start") {
            public void actionPerformed(ActionEvent e) {
                smsHandler.startNotifications();
                gui.addText("Connected!");
            }
        });
        gui.addButton(new AbstractAction("Stop") {
            public void actionPerformed(ActionEvent e) {
                smsHandler.stopNotifications();
                gui.addText("Disconnected!");
            }
        });

        smsHandler.addListener(new SmsHandler.SmsListener() {
            public void smsRecv(String msg, String sender) {
                chatServer.handleMessage(new Message(sender, System.currentTimeMillis(), msg));
            }
        });

        gui.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        gui.showCentered();
    }

    public static void main(String[] args) {
        try {
            new ChatApp("./ini/nrg.ini");
        } catch (IOException e) {
            System.err.println("Nie mozna otworzyc pliku konfiguracyjnego: " + e);
        }
    }
}
