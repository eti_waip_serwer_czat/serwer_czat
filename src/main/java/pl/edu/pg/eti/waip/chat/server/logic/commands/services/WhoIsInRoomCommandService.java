package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;
import pl.edu.pg.eti.waip.chat.server.logic.messengers.Messenger;
import pl.edu.pg.eti.waip.chat.server.logic.rooms.ChatRoom;

import java.util.Iterator;
import java.util.Set;

public class WhoIsInRoomCommandService implements CommandService {
    private final CommandServicesResource resource;

    public WhoIsInRoomCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        System.out.println("??????aasd");
        StringBuffer messageBuilder = new StringBuffer();

        ChatRoom chatRoomWithMessenger = resource.chatRoomManager.getChatRoomWithMessenger(message.sender);
        messageBuilder.append("room: " + chatRoomWithMessenger.name + ChatServer.ENDL);

        messageBuilder.append("who: ");
        Set messengers = chatRoomWithMessenger.getListeningMessengers();
        Iterator iterator = messengers.iterator();
        while (iterator.hasNext()) {
            String messengerId = (String) iterator.next();
            Messenger messenger = resource.messengerManager.getMessengerByNumber(messengerId);
            messageBuilder.append(messenger.name);
            messageBuilder.append(ChatServer.ENDL);
        }
        resource.messageSender.send(messageBuilder.toString(), message.sender);

        System.out.println(message.sender + " listed who is in room " + chatRoomWithMessenger.name);
    }
}