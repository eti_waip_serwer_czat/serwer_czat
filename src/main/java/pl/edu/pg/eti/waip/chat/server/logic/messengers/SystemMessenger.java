package pl.edu.pg.eti.waip.chat.server.logic.messengers;

public class SystemMessenger extends Messenger {
    public final String systemId;

    public SystemMessenger(String systemId, String name) {
        super(name);
        this.systemId = systemId;
    }
}
