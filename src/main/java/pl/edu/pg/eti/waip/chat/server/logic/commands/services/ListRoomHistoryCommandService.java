package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;
import pl.edu.pg.eti.waip.chat.server.logic.rooms.ChatRoom;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class ListRoomHistoryCommandService implements CommandService {
    private final CommandServicesResource resource;

    public ListRoomHistoryCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        StringTokenizer stringTokenizer = new StringTokenizer(message.content);
        String command = (String) stringTokenizer.nextElement();
        String param1 = (String) stringTokenizer.nextElement();

        int lineCount = 0;
        try {
            lineCount = Integer.parseInt(param1);
        } catch (NumberFormatException e) {}

        String roomName = resource.chatRoomManager.getChatRoomWithMessenger(message.sender).name;
        List historyForRoom = resource.messagesHistory.getHistoryForRoom(roomName, lineCount);

        StringBuffer messageBuilder = new StringBuffer();

        Iterator iterator = historyForRoom.iterator();
        while (iterator.hasNext()) {
            String line = (String) iterator.next();
            messageBuilder.append(line);
            messageBuilder.append(ChatServer.ENDL);
        }

        resource.messageSender.send(messageBuilder.toString(), message.sender);
    }
}