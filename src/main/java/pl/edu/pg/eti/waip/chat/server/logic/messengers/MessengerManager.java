package pl.edu.pg.eti.waip.chat.server.logic.messengers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MessengerManager {

    private Map/*<String, Messenger>*/ messengerMap;

    public MessengerManager() {
        messengerMap = new HashMap();
    }

    public Messenger getMessengerByNumber(String messengerNumber) {
        if (!messengerMap.containsKey(messengerNumber)) {
            messengerMap.put(messengerNumber, new PhoneMessenger(messengerNumber, messengerNumber));
        }
        return (Messenger) messengerMap.get(messengerNumber);
    }

    public Collection getMessangers() {
        return messengerMap.values();
    }

    public void setMessenger(SystemMessenger messenger) {
        messengerMap.put(messenger.systemId, messenger);
    }

    public void setMessenger(PhoneMessenger messenger) {
        messengerMap.put(messenger.number, messenger);
    }

    public String setMessengerName(String messengerNumber, String name) {
        Messenger oldMessenger = getMessengerByNumber(messengerNumber);
        PhoneMessenger messenger = new PhoneMessenger(messengerNumber, name);
        setMessenger(messenger);
        return oldMessenger.name;
    }
}
