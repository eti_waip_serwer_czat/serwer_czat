package pl.edu.pg.eti.waip.chat.server.logic.commands.services;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.logic.ChatServer;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandService;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;

public class CommandHelpCommandService implements CommandService {
    private final static String HELP_CONTENT =
            "commands: " +
                    "/say <message>" + ChatServer.ENDL +
                    "/name <name>" + ChatServer.ENDL +
                    "/join <room>" + ChatServer.ENDL +
                    "/history <number>" + ChatServer.ENDL +
                    "/list" + ChatServer.ENDL +
                    "/who" + ChatServer.ENDL +
                    "/help" + ChatServer.ENDL;

    private final CommandServicesResource resource;

    public CommandHelpCommandService(CommandServicesResource resource) {
        this.resource = resource;
    }

    public void execute(Message message) {
        resource.messageSender.send(HELP_CONTENT, message.sender);

        System.out.println(message.sender + " called help");
    }
}