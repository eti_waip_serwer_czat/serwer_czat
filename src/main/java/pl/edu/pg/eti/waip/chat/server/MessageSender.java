package pl.edu.pg.eti.waip.chat.server;

public interface MessageSender {
    void send(String messageContent, String receiver);
}
