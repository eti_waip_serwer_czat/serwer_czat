package pl.edu.pg.eti.waip.chat.server.logic.rooms;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ChatRoomManager {

    private String defaultChatRoom;
    private Map/*<String, String>*/ whereIsMessengerMap;
    private Map/*<String, ChatRoom>*/ chatRoomMap;

    public ChatRoomManager(String defaultChatRoom) {
        this.defaultChatRoom = defaultChatRoom;
        whereIsMessengerMap = new HashMap();
        chatRoomMap = new HashMap();
        chatRoomMap.put(defaultChatRoom, new ChatRoom(defaultChatRoom));
    }

    public void makeMessengerLeave(String messengerId) {
        if (whereIsMessengerMap.containsKey(messengerId)) {
            String oldRoom = (String) whereIsMessengerMap.get(messengerId);
            ((ChatRoom)chatRoomMap.get(oldRoom)).removeListeningMessenger(messengerId);
        }
    }

    public void moveMessengerToRoom(String messengerId, String roomName) {
        makeMessengerLeave(messengerId);
        whereIsMessengerMap.put(messengerId, roomName);
        getChatRoom(roomName).addListeningMessenger(messengerId);
    }

    public ChatRoom getChatRoomWithMessenger(String messengerId) {
        if (!whereIsMessengerMap.containsKey(messengerId)) {
            moveMessengerToRoom(messengerId, defaultChatRoom);
        }
        return (ChatRoom) chatRoomMap.get(whereIsMessengerMap.get(messengerId));
    }

    public ChatRoom getChatRoom(String roomName) {
        if (!chatRoomMap.containsKey(roomName)) {
            chatRoomMap.put(roomName, new ChatRoom(roomName));
        }
        return (ChatRoom)chatRoomMap.get(roomName);
    }

    public Set/*<ChatRoom>*/ getChatRooms() {
        return chatRoomMap.keySet();
    }
}
