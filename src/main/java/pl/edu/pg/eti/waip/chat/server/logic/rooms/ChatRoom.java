package pl.edu.pg.eti.waip.chat.server.logic.rooms;

import java.util.HashSet;
import java.util.Set;

public class ChatRoom {
    public final String name;
    private Set/*<String>*/ listeningMessengrs;

    public ChatRoom(String name) {
        this.name = name;
        listeningMessengrs = new HashSet();
    }

    public Set getListeningMessengers() {
        return listeningMessengrs;
    }

    public void addListeningMessenger(String messengerId) {
        listeningMessengrs.add(messengerId);
    }

    public void removeListeningMessenger(String messengerId) {
        listeningMessengrs.remove(messengerId);
    }
}
