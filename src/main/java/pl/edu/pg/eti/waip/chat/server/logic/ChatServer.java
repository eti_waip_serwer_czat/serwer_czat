package pl.edu.pg.eti.waip.chat.server.logic;

import pl.edu.pg.eti.waip.chat.server.Message;
import pl.edu.pg.eti.waip.chat.server.MessageSender;
import pl.edu.pg.eti.waip.chat.server.logic.commands.Command;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandExecutor;
import pl.edu.pg.eti.waip.chat.server.logic.commands.CommandServicesResource;
import pl.edu.pg.eti.waip.chat.server.logic.commands.MessageParser;
import pl.edu.pg.eti.waip.chat.server.logic.messengers.MessengerManager;
import pl.edu.pg.eti.waip.chat.server.logic.rooms.ChatRoomManager;

public class ChatServer {

    public final static String ENDL = "; ";//0x0D;//"\r\n";//20;//0x0a;

    private MessageSender messageSender;
    private MessengerManager messengerManager;
    private ChatRoomManager chatRoomManager;
    private MessagesHistory messagesHistory;

    private CommandServicesResource commandServicesResource;

    private CommandExecutor commandExecutor;
    private MessageParser messageParser;

    public ChatServer(MessageSender messageSender) {
        this.messageSender = messageSender;
        messengerManager = new MessengerManager();
        chatRoomManager = new ChatRoomManager("/dev/null");
        messagesHistory = new MessagesHistory();

        commandServicesResource = new CommandServicesResource(
                messageSender,
                messengerManager,
                chatRoomManager,
                messagesHistory);

        commandExecutor = new CommandExecutor(commandServicesResource);
        messageParser = new MessageParser();
    }

    public void handleMessage(Message message) {
        messagesHistory.store(message);
        Command command = messageParser.parse(message);
        if (command != null) {
            commandExecutor.execute(command);
        }
    }
}
