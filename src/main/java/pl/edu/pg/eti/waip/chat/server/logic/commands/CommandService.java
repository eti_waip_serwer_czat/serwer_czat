package pl.edu.pg.eti.waip.chat.server.logic.commands;

import pl.edu.pg.eti.waip.chat.server.Message;

public interface CommandService {
    void execute(Message message);
}
