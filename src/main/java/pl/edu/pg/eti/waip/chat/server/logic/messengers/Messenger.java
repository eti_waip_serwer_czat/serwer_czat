package pl.edu.pg.eti.waip.chat.server.logic.messengers;

public abstract class Messenger {
    public final String name;

    public Messenger(String name) {
        this.name = name;
    }
}
