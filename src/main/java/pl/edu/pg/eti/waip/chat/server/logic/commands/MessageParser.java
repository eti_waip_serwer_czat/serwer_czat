package pl.edu.pg.eti.waip.chat.server.logic.commands;

import org.apache.regexp.RE; // java 1.3 :(
import pl.edu.pg.eti.waip.chat.server.Message;

import java.util.ArrayList;
import java.util.List;

// factory of commands
public class MessageParser {

    class ParserRule {
        public final RE regex;
        public final String commandServiceName;

        ParserRule(String regex, String commandServiceName) {
            this.regex = new RE(regex);
            this.commandServiceName = commandServiceName;
        }
    }

    private List/*<ParserRule>*/ parserRules;

    public MessageParser() {
        parserRules = new ArrayList();
        parserRules.add(new ParserRule("/say .+", "say"));
        parserRules.add(new ParserRule("/name .+", "change-name"));
        parserRules.add(new ParserRule("/join .+", "join-room"));
        parserRules.add(new ParserRule("/list", "list-rooms"));
        parserRules.add(new ParserRule("/who", "who-in-room"));
        parserRules.add(new ParserRule("/history .+", "history"));
        parserRules.add(new ParserRule("/help", "help"));
        parserRules.add(new ParserRule(".+", "say"));
    }

    public Command parse(Message message) {
        for (int i = 0; i < parserRules.size(); i++) {
            ParserRule rule = (ParserRule) parserRules.get(i);
            if (rule.regex.match(message.content)) {
                return new Command(rule.commandServiceName, message);
            }
        }
        return null;
    }
}
