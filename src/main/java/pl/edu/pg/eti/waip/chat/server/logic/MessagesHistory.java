package pl.edu.pg.eti.waip.chat.server.logic;

import pl.edu.pg.eti.waip.chat.server.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesHistory {

    private List/*<Message>*/ globalHistory;
    private Map/*<String, List<String>>*/ roomHistory;

    public MessagesHistory() {
        globalHistory = new ArrayList();
        roomHistory = new HashMap();
    }

    public void store(Message message) {
        globalHistory.add(message);
    }

    public List getGlobalHistory(int num) {
        int s = globalHistory.size();
        return globalHistory.subList(s - num, s);
    }

    public List getHistoryForRoom(String roomName) {
        if (!roomHistory.containsKey(roomName)) {
            roomHistory.put(roomName, new ArrayList());
        }
        return (List) roomHistory.get(roomName);
    }

    public void storeForRoom(String roomName, String line) {
        getHistoryForRoom(roomName).add(line);
    }

    public List getHistoryForRoom(String roomName, int num) {
        List sinlgeRoomHistory = getHistoryForRoom(roomName);
        int size = sinlgeRoomHistory.size();
        if (size - num < 0 || num == 0) {
            num = size;
        }
        return sinlgeRoomHistory.subList(size - num, size);
    }
}
