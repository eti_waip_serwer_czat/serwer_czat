package pl.edu.pg.eti.waip.chat.server.app;

import com.ericsson.hosasdk.api.TpAddress;
import com.ericsson.hosasdk.api.TpAddressPlan;
import com.ericsson.hosasdk.api.TpAddressPresentation;
import com.ericsson.hosasdk.api.TpAddressRange;
import com.ericsson.hosasdk.api.TpAddressScreening;
import com.ericsson.hosasdk.api.TpHosaDeliveryTime;
import com.ericsson.hosasdk.api.TpHosaMessage;
import com.ericsson.hosasdk.api.TpHosaSendMessageError;
import com.ericsson.hosasdk.api.TpHosaSendMessageReport;
import com.ericsson.hosasdk.api.TpHosaTerminatingAddressList;
import com.ericsson.hosasdk.api.TpHosaUIMessageDeliveryType;
import com.ericsson.hosasdk.api.fw.P_UNKNOWN_SERVICE_TYPE;
import com.ericsson.hosasdk.api.hui.IpAppHosaUIManagerAdapter;
import com.ericsson.hosasdk.api.hui.IpHosaUIManager;
import com.ericsson.hosasdk.api.ui.IpAppUI;
import com.ericsson.hosasdk.api.ui.P_UI_RESPONSE_REQUIRED;
import com.ericsson.hosasdk.api.ui.TpUIEventCriteria;
import com.ericsson.hosasdk.api.ui.TpUIEventInfo;
import com.ericsson.hosasdk.api.ui.TpUIEventNotificationInfo;
import com.ericsson.hosasdk.api.ui.TpUIIdentifier;
import com.ericsson.hosasdk.utility.framework.FWproxy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class SmsHandler extends IpAppHosaUIManagerAdapter {

    private final static String SMS_SERVICE_CODE = "00";

    private final Properties frameworkProperties;
    private final String callNumber;
    private FWproxy itsFramework;
    private IpHosaUIManager itsHosaUIManager;

    Set listeners;
    private Integer assignmentID;

    public interface SmsListener {
        void smsRecv(String msg, String sender);
    }

    public SmsHandler(Properties frameworkProperties, String callNumber) {
        this.frameworkProperties = frameworkProperties;
        this.callNumber = callNumber;
        listeners = new HashSet();
        assignmentID = null;
    }

    public void addListener(SmsListener listener) {
        listeners.add(listener);
    }

    public void startNotifications() {
        try {
            itsFramework = new FWproxy(frameworkProperties);
            itsHosaUIManager = (IpHosaUIManager) itsFramework.obtainSCF("SP_HOSA_USER_INTERACTION");

            TpAddressRange originatingAddress = new TpAddressRange(TpAddressPlan.P_ADDRESS_PLAN_UNDEFINED, "*", "", "");
            TpAddressRange destinationAddress = new TpAddressRange(TpAddressPlan.P_ADDRESS_PLAN_E164, callNumber, "", "");
            TpUIEventCriteria criteria = new TpUIEventCriteria(originatingAddress, destinationAddress, SMS_SERVICE_CODE);
            assignmentID = new Integer(itsHosaUIManager.createNotification(this, criteria));
        } catch (P_UNKNOWN_SERVICE_TYPE anException) {
            System.err.println("Nie odnaleziono uslugi" + anException);
        }
    }

    public void stopNotifications() {
        if (assignmentID != null) {
            itsHosaUIManager.destroyNotification(assignmentID.intValue());
        }
        System.out.println("Disposing service manager");
        if (itsHosaUIManager != null) {
            itsFramework.releaseSCF(itsHosaUIManager);
            itsHosaUIManager = null;
        }
        System.out.println("Disposing framework");
        if (itsFramework != null) {
            itsFramework.endAccess();
            itsFramework.dispose();
            itsFramework = null;
        }
    }

    public IpAppUI reportEventNotification(TpUIIdentifier userInteraction, TpUIEventNotificationInfo eventInfo, int assignmentID) {
        Iterator iterator = listeners.iterator();
        while (iterator.hasNext()) {
            SmsListener listener = (SmsListener)iterator.next();
            listener.smsRecv(new String(eventInfo.UIEventData), eventInfo.OriginatingAddress.AddrString);
        }
        return null;
    }

    public IpAppUI reportNotification(TpUIIdentifier userInteraction, TpUIEventInfo eventInfo, int assignmentID) {
        Iterator iterator = listeners.iterator();
        while (iterator.hasNext()) {
            SmsListener listener = (SmsListener)iterator.next();
            listener.smsRecv(eventInfo.DataString, eventInfo.OriginatingAddress.AddrString);
        }
        return null;
    }

    public void sendSMS(String sender, String receiver, String messageContent) {
        TpHosaDeliveryTime deliveryTime = new TpHosaDeliveryTime();
        deliveryTime.Dummy((short) 0);

        TpHosaTerminatingAddressList recipients = new TpHosaTerminatingAddressList();
        recipients.ToAddressList = new TpAddress[]{createTpAddress(receiver)};

        TpHosaMessage message = new TpHosaMessage();
        message.Text(messageContent);

        itsHosaUIManager.hosaSendMessageReq(this,
                createTpAddress(sender),   // nadawca
                recipients,                 // odbiorcy
                null,                       // bez tematu
                message,                    // tresc wiadomosci
                TpHosaUIMessageDeliveryType.P_HUI_SMS,
                null, P_UI_RESPONSE_REQUIRED.value, false,
                deliveryTime, "");
    }

    public void hosaSendMessageErr(int assignmentID, TpHosaSendMessageError[] errorList) {
        System.out.println("\nError sending the SMS to "
                + errorList[0].UserAddress.AddrString + "(ErrorCode " + errorList[0].Error.value() + ")");
    }

    public void hosaSendMessageRes(int assignmentID, TpHosaSendMessageReport[] responseList) {
        System.out.println("\nSMS Message sent to " + responseList[0].UserAddress.AddrString);
    }

    public static TpAddress createTpAddress(String callNumber) {
        return new TpAddress(
                TpAddressPlan.P_ADDRESS_PLAN_E164, callNumber, "",
                TpAddressPresentation.P_ADDRESS_PRESENTATION_UNDEFINED,
                TpAddressScreening.P_ADDRESS_SCREENING_UNDEFINED, "");
    }
}
