package pl.edu.pg.eti.waip.chat.server.logic.messengers;

public class PhoneMessenger extends Messenger {
    public final String number;

    public PhoneMessenger(String number, String name) {
        super(name);
        this.number = number;
    }
}
