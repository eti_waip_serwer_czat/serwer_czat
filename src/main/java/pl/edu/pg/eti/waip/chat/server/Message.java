package pl.edu.pg.eti.waip.chat.server;

public class Message {
    public final String sender;
    public final long time;
    public final String content;

    public Message(String sender, long time, String content) {
        this.sender = sender;
        this.time = time;
        this.content = content;
    }
}
