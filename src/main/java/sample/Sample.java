package sample;

import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;

import com.ericsson.hosasdk.api.fw.P_UNKNOWN_SERVICE_TYPE;
import com.ericsson.hosasdk.api.hui.IpHosaUIManager;
import com.ericsson.hosasdk.api.mm.ul.IpUserLocation;
import com.ericsson.hosasdk.utility.framework.FWproxy;

public class Sample {
    private FWproxy itsFramework;
    private IpHosaUIManager itsHosaUIManager;
    private IpUserLocation itsOsaULManager;
    public SMSProcessor smsProcessor;
    public GUI theGUI;

    public Sample(GUI aGUI) {
        theGUI = aGUI;
        aGUI.setTitle("Mobilny informator");
    }

    protected void start() {
        Properties p = new Properties();
        try {
            p.load(new FileInputStream("./ini/nrg.ini"));

            // utworzenie obiektu po�rednika po��czenia
            itsFramework = new FWproxy(p);

            // pobranie referencji do klasy implementuj�cej interfejs IpHosaUIManager, s�u��cy do komunikacji z us�ug� przesy�ania prostych wiadomo�ci.
            itsHosaUIManager = (IpHosaUIManager) itsFramework.obtainSCF("SP_HOSA_USER_INTERACTION");

            // pobranie referencji do klasy implementuj�cej interfejs IpUserLocation, s�u��cy do komunikacji z us�ug� lokalizacyjn�.
            itsOsaULManager = (IpUserLocation) itsFramework.obtainSCF("P_USER_LOCATION");

        } catch (P_UNKNOWN_SERVICE_TYPE anException) {
            System.err.println("Nie odnaleziono us�ugi" + anException);
            System.exit(-1);
        } catch (IOException e) {
            System.err.println("Nie mo�na otworzy� pliku konfiguracyjnego: " + e);
        }
        smsProcessor = new SMSProcessor(itsHosaUIManager, this);
        smsProcessor.startNotifications("2222");
        theGUI.addText("Aplikacja po��czona");
    }

    public void stop() {
        if (smsProcessor != null) {
            smsProcessor.dispose();
        }
        System.out.println("Disposing service manager");
        if (itsHosaUIManager != null) {
            itsFramework.releaseSCF(itsHosaUIManager);
        }
        if (itsOsaULManager != null) {
            itsFramework.releaseSCF(itsOsaULManager);
        }
        System.out.println("Disposing framework");
        if (itsFramework != null) {
            itsFramework.endAccess();
            itsFramework.dispose();
        }
        System.exit(0);
    }
} 