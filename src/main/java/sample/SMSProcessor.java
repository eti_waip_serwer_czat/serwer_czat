package sample;

import com.ericsson.hosasdk.api.TpAddress;
import com.ericsson.hosasdk.api.TpAddressPlan;
import com.ericsson.hosasdk.api.TpAddressRange;
import com.ericsson.hosasdk.api.TpAddressPresentation;
import com.ericsson.hosasdk.api.TpAddressScreening;
import com.ericsson.hosasdk.api.TpHosaDeliveryTime;
import com.ericsson.hosasdk.api.TpHosaMessage;
import com.ericsson.hosasdk.api.TpHosaSendMessageError;
import com.ericsson.hosasdk.api.TpHosaSendMessageReport;
import com.ericsson.hosasdk.api.TpHosaTerminatingAddressList;
import com.ericsson.hosasdk.api.TpHosaUIMessageDeliveryType;
import com.ericsson.hosasdk.api.hui.IpAppHosaUIManager;
import com.ericsson.hosasdk.api.hui.IpAppHosaUIManagerAdapter;
import com.ericsson.hosasdk.api.hui.IpHosaUIManager;
import com.ericsson.hosasdk.api.ui.IpAppUI;
import com.ericsson.hosasdk.api.ui.P_UI_RESPONSE_REQUIRED;
import com.ericsson.hosasdk.api.ui.TpUIEventCriteria;
import com.ericsson.hosasdk.api.ui.TpUIEventInfo;
import com.ericsson.hosasdk.api.ui.TpUIEventNotificationInfo;
import com.ericsson.hosasdk.api.ui.TpUIIdentifier;

/**
 * Klasa odpowiedzialna za:
 *      Wysy�anie SMS�w.
 *      Obs�ug� przekazanie przychodz�cych SMS�w do klasy Informer
 */
public class SMSProcessor extends IpAppHosaUIManagerAdapter implements IpAppHosaUIManager {

    private IpHosaUIManager itsHosaUIManager;
    private Sample parent;

    /**
     * @param aHosaUIManager menad�er s�u�acy do komunikacji z Ericsson Network Resource Gateway
     * @param aParent        Referencja do klasy-rodzica Informer, kt�ra przekazuje klasie SMSProcessor wiadomo�ci do
     *                       wys�ania i przetwarza odbierane wiadomo�ci
     */
    public SMSProcessor(IpHosaUIManager aHosaUIManager, Sample aParent) {
        itsHosaUIManager = aHosaUIManager;
        this.parent = aParent;
    }

    /**
     * Funkcja dodaje nas�uch przychodzacych wiadomo�ci dla podanego numeru
     * (u�ytkownik o tym numerze nie musi istnie�, mo�e to by� numer us�ugi)
     *
     * @param sDest Adres (numer) u�ytkownika, dla kt�rego utworzona jest notyfikacja
     * @return przydzielone ID notyfikacji
     */
    public int startNotifications(String sDest) {
        // interfejs obs�uguj�cy zdarzenia pochodz�ce od bramki, w tym wypadku implementuje go klasa SMSProcessor
        IpAppHosaUIManager appHosaUIManager = this;

        // dla jakich adres�w �r�d�owych prowadzony nas�uch
        // TpAddressPlan.P_ADDRESS_PLAN_UNDEFINED oznacza, �e mo�e by� to adres w dowolnym standarzie adresowym, E164, IP, itp.,
        // znak * w drugim argumencie oznacza dowolnego nadawc�,
        // ostatnie dwa argumenty oznaczaj� podadres i spos�b prezentacji, w tym wypadku mog� mie� dowoln�nazw�
        TpAddressRange originatingAddress = new TpAddressRange(TpAddressPlan.P_ADDRESS_PLAN_UNDEFINED, "*", "", "");

        // adres docelowy
        // aplikacja zareaguje na przys�anie SMSa dla numeru lub grupy numer�w, kt�re zdefiniujemy w poni�szym obiekcie
        // system numeracyjny E164 i numer u�ytkownika podany w argumencie funkcji (sDest)
        TpAddressRange destinationAddress = new TpAddressRange(TpAddressPlan.P_ADDRESS_PLAN_E164, sDest, "", "");

        // kod us�ugi - 00 oznacza wiadomo�ci SMS (np. dla MMS kod - 01)
        String serviceCode = "00";

        // kryterium uruchomienia obs�ugi zdarzenia - przysz�a wiadomo�� SMS na numer destinationAddress od dowolnego numeru
        TpUIEventCriteria criteria = new TpUIEventCriteria(originatingAddress, destinationAddress, serviceCode);

        // utworzenie powiadamiania - przekazanie kryteri�w i klasy implementuj�cej interfejs obs�ugi zdarze�
        int assignmentId = itsHosaUIManager.createNotification(appHosaUIManager, criteria);

        return assignmentId;
    }

    /**
     * Usuni�cie notyfikacji
     *
     * @param anAssignmentId ID notyfikacji do usuni�cia - zwr�ceone przez:
     * @see SMSProcessor#startNotifications(String)
     */
    public void stopNotifications(int anAssignmentId) {
        itsHosaUIManager.destroyNotification(anAssignmentId);
    }

    /**
     * Wywo�ywana przez Ericsson Network Resource Gateway w chwili otrzymania wiadomo�ci
     *
     * @see com.ericsson.hosasdk.api.hui.IpAppHosaUIManager
     */
    public IpAppUI reportEventNotification(TpUIIdentifier anUserInteraction, TpUIEventNotificationInfo anEventInfo, int anAssignmentID) {

        String sender = anEventInfo.OriginatingAddress.AddrString;
        String receiver = anEventInfo.DestinationAddress.AddrString;
        String messageContent = new String(anEventInfo.UIEventData);

        //odes�anie wiadomo�ci "Witaj �wiecie" do nadawcy
        this.sendSMS(receiver, sender, "Witaj swiecie");

        return null;
    }

    /**
     * Wywo�ywana przez Ericsson Network Resource Gateway w chwili otrzymania wiadomo�ci
     *
     * @see com.ericsson.hosasdk.api.hui.IpAppHosaUIManager
     * @deprecated Od wersji standardu 6.0 funkcja nie b�dzie wspierana, zamiast niej nale�y u�ywa�:
     * @see IpAppHosaUIManager#reportEventNotification(TpUIIdentifier, TpUIEventNotificationInfo, int)
     * Dla zapewnienia zdgono�ci wstecznej zosta�a zaimplementowana.
     */
    public IpAppUI reportNotification(TpUIIdentifier anUserInteraction, TpUIEventInfo anEventInfo, int anAssignmentID) {
        //pobranie danych zdarzenia - nadawcy, odbiorcy i tre�ci wiadomo�ci
        String sender = anEventInfo.OriginatingAddress.AddrString;
        String receiver = anEventInfo.DestinationAddress.AddrString;
        String messageContent = anEventInfo.DataString;

        //odes�anie wiadomo�ci ,,Witaj �wiecie'' do nadawcy
        this.sendSMS(receiver, sender, "Witaj swiecie " + anEventInfo.DataString);
        return null;
    }

    /**
     * Wys�anie SMSa
     *
     * @param aSender         nadawca
     * @param aReceiver       odbiorca
     * @param aMessageContent zawarto��
     */
    public void sendSMS(String aSender, String aReceiver, String aMessageContent) {
        IpAppHosaUIManager appHosaUIManager = this;

        //typ wiadomo�ci: SMS
        TpHosaUIMessageDeliveryType deliveryType = TpHosaUIMessageDeliveryType.P_HUI_SMS;

        // Bez op�nienia
        TpHosaDeliveryTime deliveryTime = new TpHosaDeliveryTime();

        deliveryTime.Dummy((short) 0);

        //adres nadawcy
        TpAddress originatingAddress = createTpAddress(aSender);

        //adres odbiorcy
        TpAddress destinationAddress = createTpAddress(aReceiver);

        //lista odbiorc�w, w tym wypadku zawieraj�ca tylko jeden adres
        TpHosaTerminatingAddressList recipients = new TpHosaTerminatingAddressList();
        recipients.ToAddressList = new TpAddress[]{destinationAddress};

        //tre�� wiadomo�ci
        TpHosaMessage message = new TpHosaMessage();
        message.Text(aMessageContent);

        //za��danie wys�ania wiadomo�ci
        itsHosaUIManager.hosaSendMessageReq(
                appHosaUIManager,   // callback
                originatingAddress, // nadawca
                recipients,         // odbiorcy
                null,               // bez tematu
                message,            // tre�� wiadomo�ci
                deliveryType,       // rodzaj wiadomo�ci (SMS)
                null,
                P_UI_RESPONSE_REQUIRED.value, false,
                deliveryTime,
                "");
    }

    /**
     * Wywo�ywana przez Ericsson Network Resource Gateway, gdy wyst�pi� b�ad przy wysy�aniu wiadomo�ci
     *
     * @see com.ericsson.hosasdk.api.hui.IpAppHosaUIManager
     */
    public void hosaSendMessageErr(int anAssignmentID, TpHosaSendMessageError[] anErrorList) {
        System.out.println("\nError sending the SMS to "
                + anErrorList[0].UserAddress.AddrString + "(ErrorCode " + anErrorList[0].Error.value() + ")");
    }

    /**
     * Wywo�ywana przez Ericsson Network Resource Gateway, gdy wysy�anie wiadomo�ci zako�czy�o si� poprawnie
     *
     * @see com.ericsson.hosasdk.api.hui.IpAppHosaUIManager
     */
    public void hosaSendMessageRes(int anAssignmentID, TpHosaSendMessageReport[] aResponseList) {
        System.out.println("\nSMS Message sent to " + aResponseList[0].UserAddress.AddrString);
    }

    public static TpAddress createTpAddress(String aNumber) {
        return new TpAddress(
                TpAddressPlan.P_ADDRESS_PLAN_E164, aNumber,             // addres
                "",                                                     // name
                TpAddressPresentation.P_ADDRESS_PRESENTATION_UNDEFINED,
                TpAddressScreening.P_ADDRESS_SCREENING_UNDEFINED, "");  // podadres
    }
}